#[macro_use]
extern crate log;

#[cfg(target_os = "windows")]
extern crate winapi;
#[cfg(target_os = "windows")]
extern crate kernel32;
#[cfg(not(target_os = "windows"))]
extern crate nix;

use std::thread;
use std::thread::{JoinHandle};
use std::time::{Duration};
use std::sync::{mpsc, Arc};
use std::sync::mpsc::{Receiver, Sender, TryRecvError};
use std::vec::Vec;
use std::marker::Send;
use std::collections::{VecDeque, HashMap};
use std::sync::atomic::{AtomicBool, Ordering, ATOMIC_BOOL_INIT};

pub type Pump = Box<Fn() -> Result<Option<String>, &'static str>>;
pub type Peon<C> = Box<Fn(&C, usize, &str) -> Result<(), &'static str>>;

pub struct Manager {
    thread_handles: Vec<JoinHandle<()>>,
    peon_senders: HashMap<usize, Sender<ProtocolToPeon>>,
    thread_count: usize,
}

#[derive(Clone)]
enum ProtocolToPeon {
    Message(String),
    Quit,
}

#[derive(Clone)]
enum ProtocolFromPeon {
    JobDone(usize),
    Err(usize, &'static str)
}

static RUNNING: AtomicBool = ATOMIC_BOOL_INIT;

#[cfg(target_os = "windows")]
unsafe extern "system" fn windows_handle_termination(_: winapi::DWORD) -> winapi::BOOL {
    warn!(target: "SYSTEM", "Termination requested. Do not expect peons to terminate cleanly.");
    RUNNING.store(false, Ordering::SeqCst);
    
    // FIXME(lholden): We are *supposed* to be guaranteed 10 seconds to handle 
    // clean process termination, this only seems to be true for events like
    // closing the window and not hitting ctrl+c.
    thread::sleep(Duration::from_secs(10));
    winapi::TRUE
}

#[cfg(not(target_os = "windows"))]
extern fn unix_handle_sigterm(_: i32) {
    info!(target: "SYSTEM", "Termination requested.");
    info!(target: "SYSTEM", "Allowing peons to finish their current job.");
    RUNNING.store(false, Ordering::SeqCst);
}

impl Manager {
    pub fn new(thread_count: usize) -> Manager {
        Manager {
            thread_handles: vec![],
            peon_senders: HashMap::with_capacity(thread_count),
            thread_count: thread_count,
        }
    }

    #[cfg(not(target_os = "windows"))]
    fn setup_signals(&mut self) {
        use nix::sys::signal;
        use nix::sys::signal::{SigSet, SigAction, SaFlags, SigHandler};
        
        let action = SigAction::new(SigHandler::Handler(unix_handle_sigterm), SaFlags::empty(), SigSet::empty());
        unsafe {
            signal::sigaction(signal::SIGINT, &action).unwrap();
            signal::sigaction(signal::SIGHUP, &action).unwrap();
            signal::sigaction(signal::SIGTERM, &action).unwrap();
        }
    }

    #[cfg(target_os = "windows")]
    fn setup_signals(&mut self) {
        warn!(target: "SYSTEM", "Windows detected. We cannot guarantee a clean shutdown.");
        warn!(target: "SYSTEM", "Peons may not be able to finish finish their work on process termination.");
        unsafe {
            kernel32::SetConsoleCtrlHandler(Some(windows_handle_termination), winapi::TRUE);
        }
    }

    fn generate_thread<P, C>(&mut self, id: usize, generate_peon: Arc<P>, rx: Receiver<ProtocolToPeon>, pump_tx: Sender<ProtocolFromPeon>, peon_shared: C) 
    where P: Send + Sync + 'static + Fn() -> Peon<C>,
          C: Send + Sync + 'static {
        let name = format!("Peon #{}", id);
        let builder = thread::Builder::new().name(name.clone());

        self.thread_handles.push(builder.spawn(move || {
            let peon = generate_peon();

            info!(target: &name, "Ready to work.");
            while let Ok(data) = rx.recv() {
                match data {
                    ProtocolToPeon::Quit => {
                        info!(target: &name, "Quit? Be happy to.");
                        break;
                    },
                    ProtocolToPeon::Message(msg) => {
                        if let Err(err) = match peon(&peon_shared, id, &msg) {
                            Ok(()) => pump_tx.send(ProtocolFromPeon::JobDone(id)),
                            Err(err) => pump_tx.send(ProtocolFromPeon::Err(id, err)),
                        } {
                            error!(target: &name, "Couldn't reply with status: {}", err);
                        };
                    }
                }
            } 
        }).unwrap());
    }

    fn generate_work(&mut self, generate_pump: Box<Fn() -> Pump>, rx: &Receiver<ProtocolFromPeon>) {
        let pump = generate_pump();

        let mut idle_peons: VecDeque<usize> = VecDeque::with_capacity(self.thread_count);
        for key in self.peon_senders.keys() {
            idle_peons.push_back(key.clone());
        }

        RUNNING.store(true, Ordering::SeqCst);
        while RUNNING.load(Ordering::SeqCst) {
            match rx.try_recv() {
                Err(TryRecvError::Empty) => {},
                Err(TryRecvError::Disconnected) => {
                    panic!("Unexpected error. Receiver is Disconnected");
                },
                Ok(ProtocolFromPeon::JobDone(id)) => {
                    idle_peons.push_back(id);
                },
                Ok(ProtocolFromPeon::Err(id, err)) => {
                    error!(target: "Manager", "Peon #{} returned an error: {}", id, err);
                    idle_peons.push_back(id);
                }
            }
            while !idle_peons.is_empty() {
                match pump() {
                    Err(err) => {
                        error!(target: "Manager", "Pump returned an error: {}", err);
                        break;
                    },
                    Ok(None) => break,
                    Ok(Some(work)) => {
                        let id = idle_peons.pop_front().unwrap();
                        self.peon_senders[&id].send(ProtocolToPeon::Message(work)).unwrap();
                    }
                }
            }

            thread::sleep(Duration::from_secs(1));
        }
    }

    pub fn start<P, C>(&mut self, generate_pump: Box<Fn() -> Pump>, generate_peon: P, peon_shared: C)
    where P: Send + Sync +'static + Fn() -> Peon<C>,
          C: Send + Clone + Sync +'static {
        let generate_peon = Arc::new(generate_peon);

        info!(target: "Manager", "Spawning Peons");

        let (pump_tx, pump_rx) = mpsc::channel::<ProtocolFromPeon>();
        for id in 0..(self.thread_count) {
            let (tx, rx) = mpsc::channel::<ProtocolToPeon>();
            self.peon_senders.insert(id, tx.clone());
            self.generate_thread(id, generate_peon.clone(), rx, pump_tx.clone(), peon_shared.clone());
        }

        self.setup_signals();
        self.generate_work(generate_pump, &pump_rx);
        self.shutdown();
    }

    fn shutdown(&mut self) {
        info!(target: "Manager", "Announcing shutdown.");
        for (_, peon) in &self.peon_senders {
            let _ = peon.send(ProtocolToPeon::Quit);           
        }
        while let Some(handle) = self.thread_handles.pop() {
            let _ = handle.join();
        }
        info!(target: "Manager", "Shutdown complete.");
    }
}
