#[macro_use]
extern crate log;
extern crate simplelog;

extern crate rusty_peon;

use rusty_peon::{Pump, Peon};
use std::thread;
use std::time::{Duration};
use rusty_peon::Manager;
use simplelog::{TermLogger, LogLevelFilter};

#[derive(Clone)]
struct PeonShared {
}
unsafe impl Send for PeonShared {}
unsafe impl Sync for PeonShared {}

fn create_pump() -> Pump {
    Box::new(move || {
        Ok(Some("Hi!".into()))
    })
}

fn create_peon() -> Peon<PeonShared> {
    Box::new(move |shared, id, msg| {
        let name = format!("Peon #{}", id);
        info!(target: &name, "I found stuff: {}", msg);

        // Sleep based on our id just to spread things out in this example
        thread::sleep(Duration::from_secs(id as u64));
        Ok(())
    })
}

fn main() {
    TermLogger::init(LogLevelFilter::max()).unwrap();
    let mut m = Manager::new(5);
    let s = PeonShared{};
    m.start(Box::new(create_pump), create_peon, s);
}
